﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.UIElements;

public class catRead : MonoBehaviour
{

    public string taName;
    public TextAsset ta;
    public string[] data;
    public List<string> ind, chn;
    public List<Sprite> spt;

    public Transform par;
    public UnityEngine.UI.Image[] img;
    public Sprite cover;
    public Sprite[] covers;
    public int covIn;

    public KeyCode tstR;
    public float waiting = 5;

    [System.Serializable]
    public class swtc
    {
        public Transform[] tr = new Transform[2];
    }

    public List<swtc> st;
    public int times;


    public float dly;
    public Vector3 scl, scl2;

    public bool begin;
    public static catRead sc;

    public crMng cm;

    private void Awake()
    {
        sc = this;
        //hlg().enabled = false;
    }

    private void OnEnable()
    {

        //hlg().enabled = false;

        if (!string.IsNullOrEmpty(taName))
            ta = Resources.Load<TextAsset>(taName);

        ind.Clear();
        chn.Clear();
        spt.Clear();
        st.Clear();
        if (Scoring.scr)
            if (Scoring.scr.transform.childCount > 0)
                Scoring.scr.clear();
        //anm(true);


        if (covers.Length > 0)
            cover = covers[covIn];

        if (par)
        {
            img = new UnityEngine.UI.Image[par.childCount];
            for (int i = 0; i < par.childCount; i++)
            {
                img[i] = par.GetChild(i).GetChild(0).GetComponent<UnityEngine.UI.Image>();
            }
        }


        data = ta.text.Split('\n');
        for (int i = 0; i < data.Length; i++)
        {
            if (!string.IsNullOrEmpty(data[i]))
            {
                ind.Add(data[i].Split(';')[0]);
                chn.Add(data[i].Split(';')[1]);
                spt.Add(Resources.Load<Sprite>(ta.name + "/" + ind[i]));
            }
        }

        for (int i = 0; i < img.Length; i++)
        {

            if (cover)
                img[i].transform.GetChild(1).GetComponent<UnityEngine.UI.Image>().sprite = cover;

            int a = Random.Range(0, spt.Count);
            img[i].sprite = spt[a];
            int idcx = System.Array.IndexOf(ind.ToArray(), spt[a].name);
            if (idcx < 0)
                OnEnable();
            img[i].transform.GetChild(0).GetComponent<Text>().text = chn[idcx];
            spt.RemoveAt(a);
        }

        StartCoroutine(randNow(waiting));
        timeDown.sc.valMax = timeDown.sc.valNow = (waiting - 1) + (waiting);
        timeDown.sc.after = "/" + timeDown.sc.valMax.ToString("f0") + " Sec";
    }

    public HorizontalLayoutGroup hlg()
    {
        return GetComponent<HorizontalLayoutGroup>();
    }

    public void anm(bool open)
    {

        for (int i = 0; i < img.Length; i++)
        {
            //img[i].transform.GetChild(1).GetComponent<Animator>().SetBool("close", !open);
            img[i].transform.GetChild(1).GetComponent<Animator>().Play(open ? "open" : "close 0");

        }
    }

    IEnumerator randNow(float d)
    {

        yield return new WaitForSeconds(d - 1);
        int c = Random.Range(0, img.Length);
        GameObject g = Instantiate(img[c].transform.GetChild(0).gameObject, img[c].transform.GetChild(0).parent);

        Scoring.scr.trow(g);

        yield return new WaitForSeconds(d);

        anm(false);
        sfl.sc.valNow = sfl.sc.valMax = (float)times;

        yield return new WaitForSeconds(1);


        for (int i = 0; i < times; i++)
        {

            List<UnityEngine.UI.Image> im = img.ToList();

            int a = 0;


            st.Add(new swtc());
            a = Random.Range(0, im.Count);
            st[i].tr[0] = im[a].transform;
            im.RemoveAt(a);
            a = Random.Range(0, im.Count);
            st[i].tr[1] = im[a].transform;
            im.RemoveAt(a);

            //st[i].tr[0].localPosition = Vector3.zero;
            //st[i].tr[1].localPosition = Vector3.zero;

            //iTween.MoveTo(st[i].tr[0].gameObject, iTween.Hash("x", st[i].tr[1].position.x, "time", 1, "delay", i, "islocal", false));
            //iTween.MoveTo(st[i].tr[1].gameObject, iTween.Hash("x", st[i].tr[0].position.x, "time", 1, "delay", i, "islocal", false));

            //iTween.MoveTo(st[i].tr[0].gameObject, iTween.Hash("position", new Vector3(st[i].tr[1].position.x, st[i].tr[0].position.y, st[i].tr[0].position.z), "time", 1, "delay", i, "islocal", true));
            //iTween.MoveTo(st[i].tr[1].gameObject, iTween.Hash("position", new Vector3(st[i].tr[0].position.x, st[i].tr[1].position.y, st[i].tr[1].position.z), "time", 1, "delay", i, "islocal", true));
            StartCoroutine(sw(i * dly, st[i].tr));

        }



        for (int i = 0; i < st.Count; i++)
        {

        }

        StartCoroutine(starting((float)times * dly));


    }

    IEnumerator starting(float d)
    {
        yield return new WaitForSeconds(d);
        begin = true;

    }
    IEnumerator sw(float d, Transform[] t)
    {



        resetVec();
        //t[0].localPosition = Vector3.zero;
        //t[1].localPosition = Vector3.zero;

        //iTween.MoveTo(t[0].gameObject, iTween.Hash("x", t[1].position.x, "time", 1, "delay", d, "islocal", false));
        //iTween.MoveTo(t[1].gameObject, iTween.Hash("x", t[0].position.x, "time", 1, "delay", d, "islocal", false));

        int a = t[0].parent.GetSiblingIndex();
        int b = t[1].parent.GetSiblingIndex();

        yield return new WaitForSeconds(d);
        hlg().enabled = false;

        //resetVec();
        //hlg().enabled = false;


        //t[0].localPosition = Vector3.zero;
        //t[1].localPosition = Vector3.zero;

        //t[0].parent.SetAsLastSibling();
        //t[1].parent.SetAsFirstSibling();


        //iTween.MoveAdd(t[0].GetChild(1).gameObject, iTween.Hash("y", 0.1f, "time", dly/2, "delay", 0, "islocal", true));
        //iTween.MoveAdd(t[1].GetChild(1).gameObject, iTween.Hash("y", 0.1f, "time", dly/2, "delay", 0, "islocal", true));

        //iTween.MoveTo(t[0].GetChild(1).gameObject, iTween.Hash("y", 0, "time", dly/2, "delay", dly / 2, "islocal", true));
        //iTween.MoveTo(t[1].GetChild(1).gameObject, iTween.Hash("y", 0, "time", dly/2, "delay", dly / 2, "islocal", true));

        iTween.MoveTo(t[0].gameObject, iTween.Hash("x", t[1].position.x, "time", dly, "delay", 0, "islocal", false, "easetype", iTween.EaseType.easeInOutQuad));
        iTween.MoveTo(t[1].gameObject, iTween.Hash("x", t[0].position.x, "time", dly, "delay", 0, "islocal", false, "easetype", iTween.EaseType.easeInOutQuad));



        iTween.ScaleAdd(t[a < b ? 0 : 1].gameObject, iTween.Hash("amount", scl, "time", dly / 2, "delay", 0));
        iTween.ScaleAdd(t[a > b ? 0 : 1].gameObject, iTween.Hash("amount", scl2, "time", dly / 2, "delay", 0));

        iTween.ScaleTo(t[0].gameObject, iTween.Hash("scale", Vector3.one, "time", dly / 2, "delay", dly / 2));
        iTween.ScaleTo(t[1].gameObject, iTween.Hash("scale", Vector3.one, "time", dly / 2, "delay", dly / 2));

        t[0].parent.SetSiblingIndex(b);
        t[1].parent.SetSiblingIndex(a);

        //t[0].localPosition = Vector3.zero;
        //t[1].localPosition = Vector3.zero;
        //resetVec();

        sfl.sc.valNow = sfl.sc.valNow - 1;


    }

    public void resetVec()
    {
        for (int i = 0; i < img.Length; i++)
            img[i].transform.localPosition = Vector3.zero;
    }

    // Start is called before the first frame update
    void Start()
    {

        hlg().enabled = true;

    }


    // Update is called once per frame
    void Update()
    {

        //if (hlg().enabled)
        //    hlg().enabled = false;


        //if (Input.GetKeyDown(tstR))
        //    Application.LoadLevel(Application.loadedLevelName);

        //if (Input.GetKeyDown(tstR))
        //    OnEnable();
    }


    public void setCategory(string category)
    {

    }
    public void setDoor(int door)
    {

    }
    public void setDelay(float delay)
    {

    }
    public void setCount(int count)
    {

    }
    public void setTime(float time)
    {

    }



}
