﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class levMod : MonoBehaviour
{

    public int modN;
    public GameObject[] modG, starG;
    public Text[] tx;
    public static string levNow = "levNow", levMax = "levMax";
    public Transform cat;
    public bool unlock;

    public int thScr;
    public float maxScr;

    // Start is called before the first frame update
    void Start()
    {

    }

    public void click()
    {
        string[] pref = name.Split(';');
        for (int i = 0; i < pref.Length; i++)
            PlayerPrefs.SetString(levMng.field[i], pref[i]);


        PlayerPrefs.SetInt(levNow, transform.GetSiblingIndex());

    }

    public static void addLev()
    {
        if (getLN() == getLM())
            PlayerPrefs.SetInt(levMax, getLN() + 1);
    }

    public static int getLM()
    {
        return PlayerPrefs.GetInt(levMax);
    }

    public static int getLN()
    {
        return PlayerPrefs.GetInt(levNow);
    }

    // Update is called once per frame
    void Update()
    {
        modN = unlock ? 2 : transform.GetSiblingIndex() == getLM() ? 1 : transform.GetSiblingIndex() < getLM()/* + cat.childCount - 2 */? 2 : 0;

        for (int i = 0; i < modG.Length; i++)
            modG[i].SetActive(i == modN);

        for (int i = 0; i < tx.Length; i++)
            tx[i].text = (transform.GetSiblingIndex() + 1).ToString();

        scoreCount.staring(starG, (float)scoreCount.getScore(transform.GetSiblingIndex()),/* (float)int.Parse(name.Split(';')[5]) * */Scoring.scoreVal);
        thScr = scoreCount.getScore(transform.GetSiblingIndex());
        maxScr = (float)int.Parse(name.Split(';')[5]) * Scoring.scoreVal;

    }
}
