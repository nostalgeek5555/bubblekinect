﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class crBtn : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        btn().onClick.AddListener(() => Scoring.scr.correction(transform.GetChild(0).GetComponent<Text>()));

    }

    public Button btn()
    {
        return GetComponent<Button>();
    }

    // Update is called once per frame
    void Update()
    {
        Scoring.scr.setCr(transform);
        if (Scoring.scr.cr)
            btn().interactable = !Scoring.scr.cr.GetCurrentAnimatorStateInfo(0).IsName("open") && catRead.sc.begin;

    }
}
