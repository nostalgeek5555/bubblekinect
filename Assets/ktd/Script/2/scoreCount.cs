﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class scoreCount : MonoBehaviour
{
    public static scoreCount sc;
    public GameObject obj;
    public float valNow, valAf, add = 1, speed = 1;
    public static float valMax;
    public string after, before, format;
    public static string scorePre = "score";

    private void Awake()
    {
        sc = this;
    }

    public void texting(string text)
    {
        getText(obj.transform).text = getText(obj.transform.GetChild(0).GetChild(1)).text = text;
    }

    public Text getText(Transform t)
    {
        return t.GetComponent<Text>();
    }

    public Slider getSd()
    {
        return obj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Slider>();
    }

    public void setSd(float max, float now)
    {
        getSd().value = now;
        getSd().maxValue = max;

    }

    public void On()
    {
        obj.SetActive(true);
    }

    public void Off()
    {
        obj.SetActive(false);

    }

    public void addVal(float val)
    {
        valAf += val;
    }

    // Start is called before the first frame update
    void Start()
    {
        valMax = (crMng.script.Round + 1) * Scoring.scr.scoreNow;
    }

    public static int getScore()
    {
        return PlayerPrefs.GetInt(scorePre + levMod.getLN().ToString());
    }

    public static int getScore(int i)
    {
        return PlayerPrefs.GetInt(scorePre + i.ToString());
    }

    public static void staring(GameObject[] star, float val, float max)
    {
        for (int i = 0; i < star.Length; i++)
            star[i].SetActive((float)i / ((float)star.Length - 1) <= val / max);

    }

    // Update is called once per frame
    void Update()
    {

        if (valAf > valNow)
            valNow += add * speed * Time.deltaTime;
        else
            valNow = valAf;

        PlayerPrefs.SetInt(scorePre + levMod.getLN().ToString(), (int)valAf);

        setSd(valMax, valNow);
        obj.SetActive(valNow > 0);
        texting(before + valNow.ToString(format) + after);
        getSd().transform.parent.GetComponent<Image>().color = getSd().fillRect.GetComponent<Image>().color =
Color.Lerp(new Color(Color.red.r, Color.red.g, Color.red.b, 0.5f),
new Color(Color.green.r, Color.green.g, Color.green.b, 0.5f),
getSd().value / getSd().maxValue);
    }
}
