﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Scoring : MonoBehaviour
{

    public static Scoring scr;
    public Vector3 scl;
    public Color cl;

    public GameObject[] win, lose;

    public Animator cr;

    public static float scoreVal = 1000;
    public  float scoreNow;

    private void Awake()
    {
        scr = this;


    }

    public void trow(GameObject g)
    {
        g.transform.parent = transform;
        iTween.MoveTo(g, iTween.Hash("position", transform.position, "time", 1, "easetype", iTween.EaseType.easeInOutBack));
        iTween.ScaleTo(g, iTween.Hash("scale", scl, "time", 1, "easetype", iTween.EaseType.easeInOutBack));
        g.GetComponent<Text>().color = cl;

    }

    public void clear()
    {
        if (transform.childCount > 0)

            for (int i = 0; i < transform.childCount; i++)
                Destroy(transform.GetChild(i).gameObject);
    }

    public void setCr(Transform t)
    {
        if (transform.childCount > 0)
            if (t.GetChild(0).GetComponent<Text>().text == childText().text)
                cr = t.GetChild(1).GetComponent<Animator>();
    }

    public Text childText()
    {
        return transform.GetChild(0).GetComponent<Text>();
    }

    public void correction(Text tx)
    {



        Text t = childText();
        GameObject g = Instantiate(tx.text == t.text ? win[Random.Range(0, win.Length)] : lose[Random.Range(0, lose.Length)], tx.transform.parent.position, Quaternion.identity);

        if (tx.text == t.text)
            scoreCount.sc.addVal(scoreNow);

        if (tx.text != t.text)
            //cr.SetBool("close", false);
            cr.Play("open");

        StartCoroutine(load(3));
    }

    IEnumerator load(float d)
    {
        yield return new WaitForSeconds(d);
        //Application.LoadLevel(Application.loadedLevelName);
        crMng.script.restart();
    }

    // Start is called before the first frame update
    void Start()
    {
        scoreNow = (float)(int)(scoreVal / crMng.script.Round);

    }

    // Update is called once per frame
    void Update()
    {

    }
}
