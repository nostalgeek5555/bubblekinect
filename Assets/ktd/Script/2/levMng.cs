﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class levMng : MonoBehaviour
{

    public TextAsset ta;
    public static string[] field;
    public string[] copy;

    public GameObject obj;
    public Transform parent;

    private void Awake()
    {
        field = ta.text.Split('\n')[0].Split(';');
        copy = field;
    }

    // Start is called before the first frame update
    void Start()
    {

        for (int i = 1; i < ta.text.Split('\n').Length - 1; i++)
        {
            GameObject g = Instantiate(obj, parent);
            g.name = ta.text.Split('\n')[i];
        }

    }

    // Update is called once per frame
    void Update()
    {

    }
}
