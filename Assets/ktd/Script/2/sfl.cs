﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class sfl : MonoBehaviour
{
    public static sfl sc;
    public GameObject obj;
    public float valMax, valNow;
    public string after, before, format;

    private void Awake()
    {
        sc = this;
    }

    public void texting(string text)
    {
        getText(obj.transform).text = getText(obj.transform.GetChild(0).GetChild(1)).text = text;
    }

    public Text getText(Transform t)
    {
        return t.GetComponent<Text>();
    }

    public Slider getSd()
    {
        return obj.transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Slider>();
    }

    public void setSd(float max, float now)
    {
        getSd().value = now;
        getSd().maxValue = max;

    }

    public void On()
    {
        obj.SetActive(true);
    }

    public void Off()
    {
        obj.SetActive(false);

    }

    // Start is called before the first frame update
    void Start()
    {
        valMax = valNow;
    }

    // Update is called once per frame
    void Update()
    {
        setSd(valMax, valNow);
        obj.SetActive(valNow > 0);
        texting(before + valNow.ToString(format) + after);
        getSd().transform.parent.GetComponent<Image>().color = getSd().fillRect.GetComponent<Image>().color =
Color.Lerp(new Color(Color.red.r, Color.red.g, Color.red.b, 0.5f),
new Color(Color.green.r, Color.green.g, Color.green.b, 0.5f),
getSd().value / getSd().maxValue);
    }
}
