﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class direction : MonoBehaviour
{

    public float preLoc, curLoc;


    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        curLoc = (transform.position.x - preLoc) / Time.deltaTime;

        preLoc = transform.position.x;

        transform.localScale = new Vector3(curLoc < 0 ? 1 : curLoc > 0 ? -1 : transform.localScale.x, 1, 1);
    }
}
