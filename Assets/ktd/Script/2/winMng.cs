﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class winMng : MonoBehaviour
{

    public Text target, scoreNow, levelText;
    public float scoreVal;
    public GameObject[] star;

    // Start is called before the first frame update
    void Start()
    {
        levelText.text = "Level " + (levMod.getLN() + 1).ToString();
    }

    // Update is called once per frame
    void Update()
    {
        target.text = scoreCount.valMax.ToString("n0");
        scoreNow.text = scoreVal.ToString("n0");

        if (scoreVal < (float)scoreCount.getScore())
            scoreVal += 1 * /*scoreCount.valMax - scoreVal + (scoreVal / scoreCount.valMax)*/100 * Time.deltaTime;
        else
            scoreVal = (float)scoreCount.getScore();

        if (star.Length > 0)
            scoreCount.staring(star, scoreVal, scoreCount.valMax);
    }
}
