﻿using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using UnityEngine;

public class crMng : MonoBehaviour
{

    public GameObject[] crl;
    public GameObject inst;
    public int nu, Round;
    public static crMng script;
    public catRead cr;

    private void Awake()
    {
        script = this;
        SendMessage("set" + levMng.field[1], PlayerPrefs.GetString(levMng.field[1]));
        cr = crl[nu - 3].GetComponent<catRead>();

        for (int i = 0; i < levMng.field.Length; i++)
        {
            if (!levMng.field[i].Contains("."))
            {
                SendMessage("set" + levMng.field[i], PlayerPrefs.GetString(levMng.field[i]));
                log("set" + levMng.field[i]);
            }

        }
    }

    public void restart()
    {
        if (inst)
            Destroy(inst);

        inst = Instantiate(crl[nu - 3], transform);
        inst.transform.SetAsFirstSibling();

        if (Round > 0)
        {
            Round--;

        }
        else
        {

            if (scoreCount.sc.valAf > 0)
            {
                levMod.addLev();
                SendMessage("setIndex", 3);
            }
            else
            {
                SendMessage("setIndex", 4);

            }

            SendMessage("PerformTransition");

        }

    }

    public int getInt(string s)
    {
        return int.Parse(s);
    }

    public float getFloat(string s)
    {
        return (float)getInt(s);
    }

    // Start is called before the first frame update
    void Start()
    {
        restart();
        round.sc.valMax = (float)Round;
    }

    // Update is called once per frame
    void Update()
    {
        if (round.sc.valMax == 0)
            round.sc.valMax = (float)Round;

        round.sc.valNow = (float)Round;

    }

    public void log(string s)
    {
        UnityEngine.Debug.Log(s);
    }


    public void setCategory(string category) { cr.taName = category; log("set string Category"); }
    //public void setDoor(int door) { nu = door; log("set int Door"); }
    //public void setDelay(float delay) { cr.waiting = delay; log("set float Delay"); }
    //public void setCount(int count) { cr.times = count; log("set int Count"); }
    //public void setTime(float time) { cr.dly = time; log("set float Time"); }
    //public void setRound(int round) { Round = round; log("set int Round"); }

    public void setDoor(string door) { nu = getInt(door); log("set string Door"); }
    public void setDelay(string delay) { cr.waiting = getFloat(delay); log("set string Delay"); }
    public void setCount(string count) { cr.times = getInt(count); log("set string Count"); }
    public void setTime(string time) { cr.dly = getFloat(time); log("set string Time"); }
    public void setRound(string round) { Round = getInt(round); log("set string Round"); }






}
