﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Combine : MonoBehaviour
{

    public ReflectionProbe rp;
    public RawImage ri;

    public Vector2 scale;
    public Vector2 offset;
    public int sourceDepthSlice;
    public int destDepthSlice;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        //Graphics.Blit(ri.mainTexture, rp.realtimeTexture, scale, offset, sourceDepthSlice, destDepthSlice);
        Graphics.Blit(ri.mainTexture, rp.realtimeTexture);
    }
}
