﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class spawnBubble : MonoBehaviour
{

    public GameObject bbl;
    public float timeNow, timeBack;
    public int count;

    public Transform L, R, T, D;

    // Start is called before the first frame update
    void Start()
    {
        timeBack = timeNow;
        spawn();
    }

    public Vector3 pos()
    {
        return new Vector3(Random.Range(L.position.x, R.position.x), Random.Range(T.position.y, D.position.y), 0);
    }

    public void spawn()
    {
        for (int i = 0; i < count; i++)
        {
            GameObject g = Instantiate(bbl, transform);
            g.SetActive(true);
            g.transform.position = pos();
        }
    }

    // Update is called once per frame
    void Update()
    {
        if (timeNow > 0)
        {
            timeNow -= 1 * 1 * Time.deltaTime;
        }
        else
        {
            spawn();
            timeNow = timeBack;
        }
    }
}
