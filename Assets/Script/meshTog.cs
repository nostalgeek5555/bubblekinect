﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class meshTog : MonoBehaviour
{
    public KeyCode kc;


    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(kc))
            GetComponent<MeshRenderer>().enabled = !GetComponent<MeshRenderer>().enabled;
    }
}
