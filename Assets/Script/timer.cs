﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class timer : MonoBehaviour
{

    public float duration, timeBack;



    public itemMngr im()
    {
        return GetComponent<itemMngr>();
    }


    // Start is called before the first frame update
    void Start()
    {
        timeBack = duration;
    }

    // Update is called once per frame
    void Update()
    {
        duration += 1 * (timeBack > 0 ? -1 : 1) * Time.deltaTime;
        im().set(duration, timeBack, "f1");
        if (timeBack == 0)
            lbSave.score = duration;
            FirebaseManager.Instance.score = duration;

        if (duration < 0)
        {
            FindObjectOfType<result>().win();
            duration = 0;
        }

    }
}
