﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class bubbleCounter : MonoBehaviour
{

    public int count, record;
    public move[] mv;

    public itemMngr im()
    {
        return GetComponent<itemMngr>();
    }

    public void rec(int i)
    {
        if (i - record == 1)
            record = i;
        else
            FindObjectOfType<result>().lose();
    }

    public void less()
    {
        count--;
    }

    public void less(int i)
    {
        count = count - i;
    }

    // Start is called before the first frame update
    void Start()
    {
        mv = FindObjectsOfType<move>();
        count = mv.Length;

        for (int i = 0; i < count; i++)
            mv[i].gameObject.AddComponent<lessCounter>();

    }

    // Update is called once per frame
    void Update()
    {
        im().set((float)count, 0, "n0");

        if (!FindObjectOfType<move>())
            FindObjectOfType<result>().win();

    }
}
