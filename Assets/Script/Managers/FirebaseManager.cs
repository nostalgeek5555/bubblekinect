﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Firebase;
using Firebase.Auth;
using Firebase.Database;
using System.Linq;

public class FirebaseManager : MonoBehaviour
{
    public static FirebaseManager Instance;

    //Operator login data
    [Header("Operator Data")]
    public string operatorEmail = "untukproject01@gmail.com";
    public string operatorPassword = "123456";

    //Firebase variables
    [Header("Firebase")]
    public DependencyStatus dependencyStatus;
    public FirebaseAuth auth;
    public FirebaseUser User;
    public DatabaseReference DBreference;
    public Dispatcher dispatcher;

    [Header("Current user data")]
    public string username;
    public string gender;
    public float score;

    //All user data from Firebase Realtime Database, load here
    [Header("UserData")]
    public DataSnapshot dataSnapshot = null;
    public Dictionary<string, PlayerData> playerDatas = new Dictionary<string, PlayerData>();
    public List<PlayerData> playerDataList = new List<PlayerData>();

    

    private void Awake()
    {
        DontDestroyOnLoad(gameObject);

        if (Instance == null)
        {
            Instance = this;
        }

        else
        {
            if (Instance != this)
            {
                Destroy(gameObject);
            }
        }


        //Check that all of the necessary dependencies for Firebase are present on the system
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWith(task =>
        {
            dependencyStatus = task.Result;
            if (dependencyStatus == DependencyStatus.Available)
            {
                //If they are avalible Initialize Firebase
                InitializeFirebase();
            }
            else
            {
                Debug.LogError("Could not resolve all Firebase dependencies: " + dependencyStatus);
            }
        });
    }

    private void InitializeFirebase()
    {
        //Set the authentication instance object
        auth = FirebaseAuth.DefaultInstance;
        DBreference = FirebaseDatabase.DefaultInstance.RootReference;

        OnEmailLogin(operatorEmail, operatorPassword);
    }

    public void OnEmailLogin(string email, string password)
    {
        Credential credential = EmailAuthProvider.GetCredential(email, password);
        auth.SignInWithCredentialAsync(credential).ContinueWith(task => {
            if (task.IsCanceled)
            {
                Debug.LogError("SignInWithCredentialAsync was canceled.");
                return;
            }

            if (task.IsFaulted)
            {
                Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                return;
            }

            User = task.Result;
            Debug.LogFormat("User signed in successfully: {0} ({1})",
                User.DisplayName, User.UserId);

            dispatcher.Invoke(() =>
            {
                StartCoroutine(LoadPlayerData());
            });
        });

    }

    private IEnumerator LoadPlayerData()
    {
        var DBTask = DBreference.Child("users").OrderByChild("score").GetValueAsync();

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }

        else
        {
            //Data has been retrieved
            dataSnapshot = DBTask.Result;
            Debug.Log($"snapshot data {dataSnapshot.ChildrenCount}");

            if (dataSnapshot.Exists)
            {
                if (dataSnapshot.ChildrenCount > 0)
                {
                    //Loop through every users UID and add data to player data table
                    playerDataList = new List<PlayerData>();
                    playerDatas = new Dictionary<string, PlayerData>();
                    foreach (DataSnapshot childSnapshot in dataSnapshot.Children.Reverse())
                    {
                        string username = childSnapshot.Child("username").Value.ToString();
                        string gender = childSnapshot.Child("gender").Value.ToString();
                        float score = float.Parse(childSnapshot.Child("score").Value.ToString());
                        Debug.Log($"username :: {username}");
                        Debug.Log($"gender :: {gender}");
                        Debug.Log($"score :: {score}");

                        PlayerData playerData = new PlayerData(username, gender, score);
                        playerDatas.Add(username, playerData);
                        playerDataList.Add(playerData);
                    }
                }
            }

        }
    }

    public void SignUpUser(string username, string gender, float score, Action onSignUpSuccess = null)
    {
        if (!playerDatas.ContainsKey(username))
        {
            PlayerData playerData = new PlayerData(username, gender, score);
            playerDatas.Add(username, playerData);

            string jsonData = JsonUtility.ToJson(playerData);
            DBreference.Child("users").Child(username).SetRawJsonValueAsync(jsonData).ContinueWith(task => {
                if (task.IsCompleted)
                {
                    SetCurrentUserData(username, gender, score);
                    dispatcher.Invoke(() =>
                    {
                        StartCoroutine(LoadPlayerData());
                    });

                    onSignUpSuccess?.Invoke();
                }

                else
                {
                    if (task.IsCanceled)
                    {
                        Debug.LogError("SignInWithCredentialAsync was canceled.");
                        return;
                    }
                    if (task.IsFaulted)
                    {
                        Debug.LogError("SignInWithCredentialAsync encountered an error: " + task.Exception);
                        return;
                    }
                }
            });
        }

        else
        {
            Debug.Log($"user already exist!");
        }
    }

    public IEnumerator LoginUser(string _username, Action onLoginSuccess = null)
    {
       if (playerDatas.ContainsKey(_username))
        {
            PlayerData playerData = playerDatas[_username];
            username = playerData.username;
            gender = playerData.gender;
            score = playerData.score;

            lbSave.setPN(username);

            yield return null;

            onLoginSuccess?.Invoke();
        }

       else
        {
            Debug.Log($"user not exist/user invalid");
        }
    }

    public void SetCurrentUserData(string _username, string _gender, float _score)
    {
        username = _username;
        gender = _gender;
        score = _score;

        lbSave.setPN(username);
    }

    //update player score
    public IEnumerator UpdateScore()
    {
        //Set the currently logged in user kills
        var DBTask = DBreference.Child("users").Child(username).Child("score").SetValueAsync(score);

        yield return new WaitUntil(predicate: () => DBTask.IsCompleted);

        if (DBTask.Exception != null)
        {
            Debug.LogWarning(message: $"Failed to register task with {DBTask.Exception}");
        }

        else
        {
            //score updated in firebase database
            StartCoroutine(LoadPlayerData());
        }
    }


    [Serializable]
    public class PlayerData
    {
        public string username;
        public string gender;
        public float score;

        public PlayerData(string _username, string _gender, float _score)
        {
            username = _username;
            gender = _gender;
            score = _score;
        }

        public enum Gender
        {
            men = 0,
            women = 1
        }
    }
}