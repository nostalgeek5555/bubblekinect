﻿using System;
using System.Collections.Generic;
using UnityEngine;

public class Dispatcher : MonoBehaviour
{
    private static Dispatcher instance;

    private List<Action> pending = new List<Action>();

    public static Dispatcher Instance
    {
        get
        {
            return instance;
        }
    }

    public void Invoke(Action fn)
    {
        lock (pending)
        {
            pending.Add(fn);
        }
    }

    private void InvokePending()
    {
        if (FirebaseManager.Instance != null)
        {
            lock (pending)
            {
                foreach (Action action in pending)
                {
                    action();
                }

                pending.Clear();
            }
        }
    }

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }

        else
        {
            if (instance != null && instance != this)
            {
                Destroy(gameObject);
            }
        }
    }

    private void LateUpdate()
    {
        InvokePending();
    }

}
