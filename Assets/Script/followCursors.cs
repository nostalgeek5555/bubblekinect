﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class followCursors : MonoBehaviour
{

    public bool test, off, addPoint;
    public Transform follow;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

        if (off)
            return;

        Vector3 temp = Input.mousePosition;
        temp.z = 10f; // Set this to be the distance you want the object to be placed in front of the camera.
        if (follow)
            transform.position = new Vector2(follow.position.x, follow.position.y);
        else
            this.transform.position = Camera.main.ScreenToWorldPoint(temp);

        if (test)
            if (!GameObject.FindObjectOfType<move>())
                SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.GetComponent<move>())
        {
            if (addPoint)
                collision.gameObject.SendMessage("adding");
            Destroy(collision.gameObject);

        }
    }
}
