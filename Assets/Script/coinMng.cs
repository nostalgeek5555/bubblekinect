﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class coinMng : MonoBehaviour
{

    public itemMngr im;
    public float count, back, divide;

    public bool staring;

    // Start is called before the first frame update
    void Start()
    {
        if (!im)
            im = GetComponent<itemMngr>();
    }

    public void add(Text a)
    {
        GameObject g = Instantiate(a.transform.parent.gameObject, a.transform.parent.position,/*transform.parent.parent.parent*//*a.transform.parent.parent*/Quaternion.identity);
        g.GetComponent<Canvas>().sortingOrder = 3;
        //LeanTween.move(g, transform.position, 1).setEase(LeanTweenType.easeOutSine);

        iTween.MoveTo(g, /*transform.position, 1*/iTween.Hash("position", transform.position, "time", 1, "easetype", iTween.EaseType.easeOutBack));

        Destroy(g, 1);
        count += (float)int.Parse(a.text);
        StartCoroutine(onStr(1));
    }

    IEnumerator onStr(float d)
    {
        yield return new WaitForSeconds(d);
        staring = true;
    }

    // Update is called once per frame
    void Update()
    {

        im.set(back, 0, "n0");
        lbSave.score = count;
        FirebaseManager.Instance.score = count;

        if (count < 0)
        {
            FindObjectOfType<result>().lose();
            count = 0;
        }


        if (back < count - back.ToString().Length)
        {
            if (staring)
                back += back.ToString().Length * (count - back) / divide * Time.deltaTime;
        }

        else if (back > count - back.ToString().Length)
        {
            if (staring)
                back -= back.ToString().Length * (Mathf.Abs(count - back)) / divide * Time.deltaTime;
        }
        else
        {
            back = count;
            staring = false;
        }
    }
}
