﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class itemMngr : MonoBehaviour
{

    public Image bg;
    public Slider sd;

    public Text t1, t2;

    public int len1, len2;

    public ContentSizeFitter csf;

    public string unit;

    public Image img;
    public Color c;

    public void set(float start, float end, string format)
    {
        t1.text = t2.text = start.ToString(format) + unit;
        if (end > 0)
        {
            sd.value = start / end;



        }
    }

    // Start is called before the first frame update
    void Start()
    {
        if (!t1)
            t1 = GetComponent<Text>();

        if (!t2)
            t2 = transform.GetChild(0).GetChild(1).GetComponent<Text>();

        if (!sd)
            sd = transform.GetChild(0).GetChild(0).GetChild(0).GetComponent<Slider>();

        if (!bg)
            bg = sd.fillRect.GetComponent<Image>();

        if (!csf)
            csf = GetComponent<ContentSizeFitter>();

        img = sd.fillRect.GetComponent<Image>();
        c = img.color;


    }

    // Update is called once per frame
    void Update()
    {

        img.color = Color.Lerp(new Color(1, c.g, c.b, c.a), new Color(0, c.g, c.b, c.a), sd.value);
        //img.color = Color.Lerp(Color.red, Color.green, sd.value);


        len1 = t1.text.Length;
        if (len2 != len1)
        {
            csf.enabled = true;
            len2 = len1;

        }
        else
        {
            csf.enabled = false;

        }
    }
}
