﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using BayatGames.SaveGamePro;
using UnityEngine.UI;

public class lbSave : MonoBehaviour
{

    [System.Serializable]
    public class CustomData
    {
        public string ID, format = ".json";
        public item[] points;
    }

    [System.Serializable]
    public class item
    {
        public string playerName;
        public float score;
        public int spt;
    }

    public CustomData list;

    public Transform parent;

    public listLb ll;

    public static string prefPN = "playerName", prefPc = "playerPic";
    public static string playerName;
    public static float score;
    public static int pic;

    public bool loadOnly;
    public KeyCode kc;

    public static void setPN(string n)
    {
        //PlayerPrefs.SetString(prefPN, n);
        playerName = n;
    }

    public static string getPN()
    {
        //return PlayerPrefs.GetString(prefPN);
        return playerName;
    }


    public static void setPC(int n)
    {
        //PlayerPrefs.SetInt(prefPc, n);
        pic = n;
    }

    public static int getPC()
    {
        //return PlayerPrefs.GetInt(prefPc);
        return pic;
    }




    public void save()
    {
        //Debug.Log("save");
        SaveGame.Save(getID(), list);
        //load();
    }

    public void load()
    {
        Debug.Log("load");
        list = SaveGame.Load<CustomData>(getID());

    }

    public string getID()
    {
        return list.ID + list.format;
    }

    public void remove()
    {
        Destroy(ll.gameObject);
    }

    // Start is called before the first frame update
    void Start()
    {


        SaveGameSettings settings = SaveGame.DefaultSettings;
        settings.Formatter = new BayatGames.SaveGamePro.Serialization.Formatters.Json.JsonFormatter();
        settings.Encrypt = false;
        SaveGame.DefaultSettings = settings;

        //if (!loadOnly)
        //if (!string.IsNullOrEmpty(getPN()))
        if (FindObjectOfType<objAct>())
            adding(-1);


        if (SaveGame.Exists(getID()))
            load();
        else
            save();

        if (list.points.Length > 0)
        {
            for (int i = 0; i < list.points.Length; i++)
            {
                adding(i);
            }
        }


    }
    public static void openFolder()
    {
        Application.OpenURL(SaveGame.PersistentDataPath);
    }

    public void adding(int i)
    {
        GameObject g = Instantiate(parent.transform.parent.GetChild(0).gameObject, parent);
        g.SetActive(true);
        if (i >= 0)
            g.GetComponent<listLb>().set(list.points[i].playerName, list.points[i].score, list.points[i].spt);
        else
            ll = g.GetComponent<listLb>();
    }

    public void adding()
    {
        adding(-1);
    }


    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(kc))
            openFolder();

        if (list.points.Length != parent.childCount)
            list.points = new item[parent.childCount];

        for (int i = 0; i < list.points.Length; i++)
        {
            list.points[i] = new item();
            list.points[i].playerName = parent.GetChild(i).GetComponent<listLb>().nm.text;
            list.points[i].score = parent.GetChild(i).GetComponent<listLb>().val;
            list.points[i].spt = parent.GetChild(i).GetComponent<listLb>().pidx;
        }

        if (ll)
        {
            ll.set(getPN(), score, getPC());
        }

        save();

    }
}
