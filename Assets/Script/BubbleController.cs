using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BubbleController : MonoBehaviour {
    public GameObject bubblePrefab;
    public float timeSpan = 1f;
	// Use this for initialization

	void Start () {
        StartCoroutine(Bubbling());
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    private IEnumerator Bubbling()
    {
        while (true)
        {
            yield return new WaitForSeconds(timeSpan);

            GameObject bubbleInstance = (GameObject)Instantiate(bubblePrefab);
            bubbleInstance.transform.position = new Vector3(Random.Range(-5f, 5f), 0, Random.Range(-5f, 5f));
        }
    }
}
