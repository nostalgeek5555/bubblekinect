﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class lessCounter : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnDestroy()
    {
        if (FindObjectOfType<bubbleCounter>())
        {
            FindObjectOfType<bubbleCounter>().less();
            FindObjectOfType<bubbleCounter>().rec(int.Parse(transform.GetChild(0).GetChild(0).GetComponent<Text>().text));

        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
