﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class getTexture : MonoBehaviour
{

    public RawImage ri;
    public string textName;

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        GetComponent<MeshRenderer>().material.SetTexture(textName, ri.texture);
    }
}
