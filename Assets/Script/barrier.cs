﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class barrier : MonoBehaviour
{

    public Transform R, L, T, D;
    public Vector3 pos;

    // Start is called before the first frame update
    void Start()
    {
        R = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        L = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        T = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;
        D = GameObject.CreatePrimitive(PrimitiveType.Cube).transform;

        //R.gameObject.AddComponent<Collider>();
        //L.gameObject.AddComponent<Collider>();
        //T.gameObject.AddComponent<Collider>();
        //D.gameObject.AddComponent<Collider>();

        R.GetComponent<MeshRenderer>().enabled 
            = L.GetComponent<MeshRenderer>().enabled 
            = T.GetComponent<MeshRenderer>().enabled 
            = D.GetComponent<MeshRenderer>().enabled 
            = false;

    }

    public Vector3 target(string nm)
    {
        return GameObject.Find(nm).transform.position;
    }

    // Update is called once per frame
    void Update()
    {


        pos = transform.position;

        R.transform.position = new Vector3(target("Right").x, pos.y, pos.z);
        L.transform.position = new Vector3(target("Left").x, pos.y, pos.z);

        T.transform.position = new Vector3(pos.x, target("Top").y, pos.z);
        D.transform.position = new Vector3(pos.x, target("Down").y, pos.z);


    }
}
