﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ricl : MonoBehaviour
{

    public bool over;
    public KeyCode kc = KeyCode.Mouse1;

    public void setOver(bool b)
    {
        over = b;
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(kc) && over)
            SendMessage("OpenPopup");
    }
}
