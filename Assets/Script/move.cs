﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move : MonoBehaviour
{

    public Rigidbody rb;
    public Vector3 vcMin, vcMax, vc;

    [Range(0, 8)]
    public int mode;

    public float multi, timMin, timMax, sizeMin, sizeMax;

    public GameObject splash;
    public KeyCode kc;

    // Start is called before the first frame update
    void Start()
    {

        vc = new Vector3(Random.Range(vcMin.x, vcMax.x), Random.Range(vcMin.y, vcMax.y), 0);

        float size = Random.Range(sizeMin, sizeMax);
        transform.localScale = new Vector3(size, size, size);
        rb.mass = Random.Range(0.3f, 0.8f);

        if (mode == 0)
            rb.AddForce(vc, ForceMode.Acceleration);
        if (mode == 1)
            rb.AddForce(vc, ForceMode.Force);
        if (mode == 2)
            rb.AddForce(vc, ForceMode.Impulse);
        if (mode == 3)
            rb.AddForce(vc, ForceMode.VelocityChange);

        if (mode == 4)
            rb.AddRelativeForce(vc, ForceMode.Acceleration);
        if (mode == 5)
            rb.AddRelativeForce(vc, ForceMode.Force);
        if (mode == 6)
            rb.AddRelativeForce(vc, ForceMode.Impulse);
        if (mode == 7)
            rb.AddRelativeForce(vc, ForceMode.VelocityChange);


        if (mode == 8)
            StartCoroutine(movement(multi));

    }


    private void OnDestroy()
    {
        GameObject g = Instantiate(splash, transform.position, Quaternion.identity);
        Destroy(g, 1);
    }

    IEnumerator movement(float force)
    {
        while (true)
        {
            rb.AddForce(Random.insideUnitSphere * force);
            yield return new WaitForSeconds(Random.Range(timMin, timMax));
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(kc))
            Destroy(gameObject);
    }
}
