﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class texting : MonoBehaviour
{

    public GameObject textG;
    public bool sort;
    public int randMin, randMax;

    public Text textU;


    private void Awake()
    {
        transform.SetSiblingIndex(Random.Range(0, transform.parent.childCount));

    }

    // Start is called before the first frame update
    void Start()
    {
        GameObject g = Instantiate(textG, transform);
        textU = g.transform.GetChild(0).GetComponent<Text>();
        textU.text = sort ? (transform.GetSiblingIndex() + 1).ToString() : Random.Range(randMin, randMax).ToString();
        textU.color = int.Parse(textU.text) >= 0 ? Color.black : Color.red;
        //transform.SetSiblingIndex(Random.Range(0, transform.parent.childCount));
    }



    // Update is called once per frame
    void Update()
    {

    }
}
