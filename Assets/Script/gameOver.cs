﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class gameOver : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {

    }

    private void OnEnable()
    {
        OnGameOver();
    }

    public void restart()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    public void exit()
    {
        Application.Quit();
    }

    public void back()
    {
        SendMessage("PerformTransition");
    }

    public void OnGameOver()
    {
        if (FirebaseManager.Instance != null)
        {
            StartCoroutine(FirebaseManager.Instance.UpdateScore());
        }
    }

    // Update is called once per frame
    void Update()
    {

    }
}
