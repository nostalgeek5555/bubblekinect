﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class listLb : MonoBehaviour
{

    public Text nm, sc, rank;
    public Image pic;
    public Sprite[] spt;
    public bool highToLow;

    public string format, unit, rep;

    public float val;
    public int pidx, idxStart, idxEnd;

    public Scrollbar sb;

    public KeyCode kc = KeyCode.Delete;
    public bool over;

    public int getSc()
    {
        return int.Parse(sc.text.Replace(rep, string.Empty).Replace(unit, string.Empty));
    }


    // Start is called before the first frame update
    void Start()
    {

    }

    public void set(string nam, float sco, int pc)
    {
        nm.text = nam;
        val = sco;
        pidx = pc;
    }

    public void setOver(bool b)
    {
        over = b;
    }

    public void del()
    {

        if (over)
            Destroy(gameObject);
    }

    // Update is called once per frame
    void Update()
    {

        if (Input.GetKeyDown(kc) && over)
            Destroy(gameObject);

        idxStart = transform.GetSiblingIndex();
        if (idxEnd != idxStart)
        {
            sb.value = (((((float)idxStart / (float)transform.parent.childCount) * -1f) + 1f) /*- 0.5f*/)/* * 2f*/;
            idxEnd = idxStart;
        }

        if (FindObjectOfType<lbSave>().ll == this)
        {
            Image img = transform.GetChild(0).GetComponent<Image>();
            //img.color = new Color(255, img.color.g, img.color.b, img.color.a);
            img.color = new Color(img.color.r, img.color.g, 0, img.color.a);

        }

        if (transform.GetSiblingIndex() > 0)
        {
            if (highToLow)
            {
                if (val > transform.parent.GetChild(transform.GetSiblingIndex() - 1).GetComponent<listLb>().val)
                    transform.SetSiblingIndex(transform.GetSiblingIndex() - 1);
            }

            else
            {
                if (val < transform.parent.GetChild(transform.GetSiblingIndex() - 1).GetComponent<listLb>().val)
                    transform.SetSiblingIndex(transform.GetSiblingIndex() - 1);
            }

        }

        sc.text = val.ToString(format) + unit;
        pic.sprite = spt[pidx];

        rank.text = (transform.GetSiblingIndex() + 1).ToString("d2");

    }
}
