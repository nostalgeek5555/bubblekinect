﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class result : MonoBehaviour
{

    public KeyCode kc = KeyCode.O;

    public void win()
    {
        transform.GetChild(0).gameObject.SetActive(true);
        ls().transform.parent = transform.GetChild(0).Find("Canvas1");
        ls().save();
        ls().transform.localPosition = Vector3.zero;
    }

    public void lose()
    {
        transform.GetChild(1).gameObject.SetActive(true);
        ls().remove();
        ls().transform.parent = transform.GetChild(1).Find("Canvas1");
        ls().transform.localPosition = Vector3.zero;

    }

    public lbSave ls()
    {
        return FindObjectOfType<lbSave>();
    }

    // Start is called before the first frame update
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(kc))
            lbSave.openFolder();
    }
}
