﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class objAct : MonoBehaviour
{

    public GameObject[] objList;
    public bool onAwake, onStart, onDis, onDes, status;

    private void Awake()
    {
        if (onAwake)
            activated(status);

    }

    private void Start()
    {

        if (onStart)
            activated(status);
    }

    private void OnDisable()
    {

        if (onDis)
            activated(status);
    }

    private void OnDestroy()
    {
        if (onDes)
            activated(status);

    }

    public void activated(bool b)
    {
        for (int i = 0; i < objList.Length; i++)
            objList[i].SetActive(b);
    }


    // Update is called once per frame
    void Update()
    {

    }
}
