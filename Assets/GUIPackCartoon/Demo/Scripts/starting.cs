﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class starting : MonoBehaviour
{
    public Popup popup;
    [SerializeField] public InputField user;
    public AnimatedButton loginButton;
    public AnimatedButton signupButton;
    public Toggle menToggle;
    public Toggle womenToggle;
    
    // Start is called before the first frame update
    void Start()
    {
        if (loginButton != null)
        {
            loginButton.onClick.RemoveAllListeners();
            loginButton.onClick.AddListener(() =>
            {
                OnUserLogin();
            });
        }
        
        if (signupButton != null)
        {
            signupButton.onClick.RemoveAllListeners();
            signupButton.onClick.AddListener(() =>
            {
                OnUserSignup();
            });
        }
        
    }

    public void OnUserLogin()
    {
        if (FirebaseManager.Instance != null)
        {
            StartCoroutine(FirebaseManager.Instance.LoginUser(user.text, popup.Close));
        }
    }

    public void OnUserSignup()
    {
        if (FirebaseManager.Instance != null)
        {
            if (menToggle != null)
            {
                if (menToggle.isOn)
                {
                    FirebaseManager.Instance.SignUpUser(user.text, FirebaseManager.PlayerData.Gender.men.ToString(), 0, popup.Close);
                }
            }
            

            else if (womenToggle != null)
            {
                if (womenToggle.isOn)
                {
                    FirebaseManager.Instance.SignUpUser(user.text, FirebaseManager.PlayerData.Gender.women.ToString(), 0, popup.Close);
                }
            }
        }
    }

    public void setPN(int i)
    {
        lbSave.setPC(i);
    }

    private void OnDestroy()
    {
        FindObjectOfType<objAct>().activated(true);
    }

    // Update is called once per frame
    void Update()
    {
        //lbSave.setPN(user.text);

        if (loginButton != null)
        {
            loginButton.interactable = !string.IsNullOrEmpty(user.text);
        }
        
    }
}
