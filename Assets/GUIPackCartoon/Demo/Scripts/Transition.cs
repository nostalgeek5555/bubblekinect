﻿// Copyright (C) 2015 ricimi - All rights reserved.
// This code can only be used under the standard Unity Asset Store End User License Agreement.
// A Copy of the EULA APPENDIX 1 is available at http://unity3d.com/company/legal/as_terms.

using System.Collections;
using UnityEngine;
using UnityEngine.UI;

// This class is responsible for managing the transitions between scenes that are performed
// in the demo via a classic fade.
public class Transition : MonoBehaviour
{
    private static GameObject m_canvas;

    private GameObject m_overlay;
    public static int td;
    public static float dur;

    private void Awake()
    {
        // Create a new, ad-hoc canvas that is not destroyed after loading the new scene
        // to more easily handle the fading code.
        m_canvas = new GameObject("TransitionCanvas");
        var canvas = m_canvas.AddComponent<Canvas>();
        canvas.renderMode = RenderMode.ScreenSpaceOverlay;
        //canvas.targetDisplay = FindObjectsOfType<Transition>().Length - 1;
        canvas.targetDisplay = td;
        DontDestroyOnLoad(m_canvas);
    }

    private void Start()
    {

    }

    public static void LoadLevel(string level, float duration, Color color)
    {
        dur = duration * 5;

        td = 0;
        var fade = new GameObject("Transition");
        fade.AddComponent<Transition>();
        fade.GetComponent<Transition>().StartFade(level, duration, color);
        fade.transform.SetParent(m_canvas.transform, false);
        fade.transform.SetAsLastSibling();
        DontDestroyOnLoad(fade);

        td = 1;
        var fade2 = new GameObject("Transition");
        fade2.AddComponent<Transition>();
        fade2.GetComponent<Transition>().StartFade(level, duration, color);
        fade2.transform.SetParent(m_canvas.transform, false);
        fade2.transform.SetAsLastSibling();
        DontDestroyOnLoad(fade2);
    }

    private void Update()
    {
        //dur -= 1 * 1 * Time.deltaTime;
        //if (dur < 0)
        //    Destroy(transform.parent.gameObject);
    }

    private void StartFade(string level, float duration, Color fadeColor)
    {
        StartCoroutine(RunFade(level, duration, fadeColor));
    }

    // This coroutine performs the core work of fading out of the current scene
    // and into the new scene.
    private IEnumerator RunFade(string level, float duration, Color fadeColor)
    {
        var bgTex = new Texture2D(1, 1);
        bgTex.SetPixel(0, 0, fadeColor);
        bgTex.Apply();

        m_overlay = new GameObject();
        var image = m_overlay.AddComponent<Image>();
        var rect = new Rect(0, 0, bgTex.width, bgTex.height);
        var sprite = Sprite.Create(bgTex, rect, new Vector2(0.5f, 0.5f), 1);
        image.material.mainTexture = bgTex;
        image.sprite = sprite;
        var newColor = image.color;
        image.color = newColor;
        image.canvasRenderer.SetAlpha(0.0f);

        m_overlay.transform.localScale = new Vector3(1, 1, 1);
        m_overlay.GetComponent<RectTransform>().sizeDelta = m_canvas.GetComponent<RectTransform>().sizeDelta;
        m_overlay.transform.SetParent(m_canvas.transform, false);
        m_overlay.transform.SetAsFirstSibling();

        var time = 0.0f;
        var halfDuration = duration / 2.0f;
        while (time < halfDuration)
        {
            time += Time.deltaTime;
            image.canvasRenderer.SetAlpha(Mathf.InverseLerp(0, 1, time / halfDuration));
            yield return new WaitForEndOfFrame();
        }

        image.canvasRenderer.SetAlpha(1.0f);
        yield return new WaitForEndOfFrame();

        Application.LoadLevel(level);

        time = 0.0f;
        while (time < halfDuration)
        {
            time += Time.deltaTime;
            image.canvasRenderer.SetAlpha(Mathf.InverseLerp(1, 0, time / halfDuration));
            yield return new WaitForEndOfFrame();
        }

        image.canvasRenderer.SetAlpha(0.0f);
        yield return new WaitForEndOfFrame();

        Destroy(m_canvas);
    }
}